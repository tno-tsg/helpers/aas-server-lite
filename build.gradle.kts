import java.text.SimpleDateFormat
import java.util.*

plugins {
    id("java")
    id("org.jetbrains.kotlin.jvm") version "1.7.21"
    id("com.google.cloud.tools.jib") version "3.3.1"
    kotlin("plugin.serialization") version "1.7.21"
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

java.targetCompatibility = JavaVersion.VERSION_11

val ktorVersion = "2.3.2"

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib")
    implementation("io.ktor:ktor-server-core:$ktorVersion")
    implementation("io.ktor:ktor-server-netty:$ktorVersion")
    implementation("io.ktor:ktor-server-status-pages:$ktorVersion")
    implementation("io.ktor:ktor-serialization-gson:$ktorVersion")
    implementation("io.ktor:ktor-server-content-negotiation:$ktorVersion")

    implementation("ch.qos.logback:logback-classic:1.4.7")
    implementation("com.google.code.gson:gson:2.10.1")
    implementation("com.google.guava:guava:32.1.1-jre")

    implementation("org.eclipse.basyx:basyx.sdk:1.2.0") {
        exclude("ch.qos.logback","logback-classic")
        exclude("com.google.code.gson", "gson")
        exclude("commons-fileupload","commons-fileupload")
        exclude("commons-io","commons-io")
        exclude("javax.servlet","javax.servlet-api")
        exclude("javax.ws.rs","javax.ws.rs-api")
        exclude("org.apache.httpcomponents","httpclient")
        exclude("org.apache.httpcomponents","httpmime")
        exclude("org.apache.poi","poi-ooxml")
        exclude("org.apache.tomcat","tomcat-catalina")
        exclude("org.codehaus.janino","janino")
        exclude("org.eclipse.milo","sdk-client")
        exclude("org.eclipse.milo","sdk-server")
        exclude("org.eclipse.paho","org.eclipse.paho.client.mqttv3")
        exclude("org.glassfish.jersey.core","jersey-client")
        exclude("org.glassfish.jersey.inject","jersey-hk2")
        exclude("org.springframework.security","spring-security-oauth2-jose")
        exclude("org.springframework.security","spring-security-oauth2-resource-server")
    }

    implementation("org.litote.kmongo:kmongo-coroutine:4.8.0")

    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.5.0")


    testImplementation("org.junit.jupiter:junit-jupiter-api:5.9.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.9.2")

    testImplementation ("org.junit.jupiter:junit-jupiter-api:5.9.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.9.2")
}

repositories {
    mavenCentral()
}

tasks.withType<Test> {
    useJUnitPlatform()
}

jib {
    from {
        image = "eclipse-temurin:17-jre"

        platforms {
            platform {
                architecture = "amd64"
                os = "linux"
            }
            platform {
                architecture = "arm64"
                os = "linux"
            }
        }

    }
    to {
        val imageName = System.getenv().getOrDefault("IMAGE_NAME", "docker.nexus.dataspac.es/aas-server-lite")
        val imageTag = System.getenv().getOrDefault("IMAGE_TAG", "local").replace('/', '-')
        val imageTags = mutableSetOf(imageTag)
        if (System.getenv().containsKey("CI")) {
            imageTags += "${imageTag}-${SimpleDateFormat("yyyyMMddHHmm").format(Date())}"
        }
        image = imageName
        tags = imageTags

    }

    container {
        user = "nobody"
        creationTime.set("USE_CURRENT_TIMESTAMP")
        ports = listOf("4001/tcp")
        mainClass = "nl.tsg.aas.APIServerKt"
    }
}