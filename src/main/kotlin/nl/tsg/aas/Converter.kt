package nl.tsg.aas

import io.ktor.http.content.*
import kotlinx.coroutines.*
import nl.tsg.aas.basyx.Basyx
import org.eclipse.basyx.aas.factory.xml.XMLToMetamodelConverter
import org.eclipse.basyx.aas.metamodel.map.AssetAdministrationShell
import org.eclipse.basyx.submodel.metamodel.map.Submodel
import org.slf4j.LoggerFactory
import org.w3c.dom.Document
import org.w3c.dom.NodeList
import java.io.InputStream
import java.io.StringWriter
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path
import java.util.stream.Collectors
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.Transformer
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult
import javax.xml.xpath.XPath
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory
import kotlin.io.path.isDirectory
import kotlin.io.path.outputStream
import kotlin.io.path.readBytes
import kotlin.text.Regex.Companion.escape

data class XPathReplacement(val name: String, val xpath: String, val value: String)
data class ConverterResult(val aasId: String, val result: String, val reason: String?, val details: Any?) {
  constructor(aasId: String, exception: Exception, debug: Boolean) : this(
    aasId,
    "Error",
    exception.message,
    if (debug) exception.stackTraceToString() else null
  )
}

data class ReplaceResult(val name: String, val xpath: String, val value: String, val matches: Int)


class Converter(private val basyx: Basyx) {
  private val xPath: XPath = XPathFactory.newInstance().newXPath()
  companion object {
    private val LOG = LoggerFactory.getLogger(Converter::class.java)
  }

  suspend fun handleRequest(multipartData: MultiPartData, debug: Boolean): Any {
    var xml = ""
    var replaceMap: Map<String, List<XPathReplacement>>? = null
    var generateSubmodelIdentification = true
    multipartData.forEachPart { part ->
      when (part) {
        is PartData.FormItem -> {
          when (part.name) {
            "generateSubmodelIdentification" -> {
              generateSubmodelIdentification = part.value.toBoolean()
            }
            else -> {
              LOG.warn("Unknown part name \"${part.name}\" in request, ignoring")
            }
          }
        }
        is PartData.FileItem -> {
          when (part.name) {
            "aasx" -> {
              withContext(Dispatchers.IO) {
                val tmpFile = Files.createTempFile(null, null)
                part.streamProvider().use { input ->
                  tmpFile.outputStream().use { output ->
                    input.copyTo(output)
                  }
                }
                xml = FileSystems.newFileSystem(tmpFile, null as ClassLoader?).use { fs ->
                  val aasxXmlPaths = findXmlFiles(fs.getPath("/"))
                  if (aasxXmlPaths.isEmpty()) {
                    throw BadRequestException("No \".aas.xml\" file found in the AASX file")
                  }
                  if (aasxXmlPaths.size > 1) {
                    LOG.warn("Multiple \".aas.xml\" files found in the AASX file, using the first (${aasxXmlPaths.first()})")
                  }
                  aasxXmlPaths.first().readBytes().decodeToString()
                }
              }
            }
            "replacemap" -> {
              replaceMap = readReplacementMap(part.streamProvider())
            }
            else -> {
              LOG.warn("Unknown part name \"${part.name}\" in request, ignoring")
            }
          }
        }
        else -> {
          LOG.warn("Unknown part type \"${part.javaClass.simpleName}\" in request, ignoring")
        }
      }
    }
    if (xml === "") {
      throw BadRequestException("No \"aasx\" part found in request, make sure to create a form-data request with at least 1 \"aasx\" part")
    }

    val result: Any = if (replaceMap != null) {
      replaceMap!!.map { (idShort, replacementList) ->
        try {
          val builderFactory = DocumentBuilderFactory.newInstance()
          val builder = builderFactory.newDocumentBuilder()
          val xmlDocument = builder.parse(xml.byteInputStream())
          val replaceResults = replacementList.map {
            val matches = replace(xmlDocument, it.xpath, it.value)
            ReplaceResult(it.name, it.xpath, it.value, matches)
          }

          val tFactory = TransformerFactory.newInstance()
          val transformer: Transformer = tFactory.newTransformer()

          val source = DOMSource(xmlDocument)
          val stringWriter = StringWriter()
          val result = StreamResult(stringWriter)
          transformer.transform(source, result)

          xmlToMongo(stringWriter.toString(), generateSubmodelIdentification, debug).let {
            if (debug && it.result == "Success") {
              it.copy(details = replaceResults)
            } else {
              it
            }
          }
        } catch (e: Exception) {
          LOG.warn("Failed to create AAS with ID $idShort: ${e.message}")
          ConverterResult("Unknown", e, debug)
        }
      }
    } else {
      xmlToMongo(xml, generateSubmodelIdentification, debug)
    }
    return result
  }

  private fun findXmlFiles(path: Path): Set<Path> {
    return if (path.isDirectory()) {
      Files.list(path).map { findXmlFiles(it) }.collect(Collectors.toSet()).flatten().toSet()
    } else if (path.toString().endsWith(".aas.xml")) {
      setOf(path)
    } else {
      emptySet()
    }
  }

  private suspend fun xmlToMongo(
    xml: String,
    generateSubmodelIdentification: Boolean,
    debug: Boolean
  ): ConverterResult {
    var aas: AssetAdministrationShell? = null
    return try {
      val converter = XMLToMetamodelConverter(xml)
      aas = converter.parseAAS().first() as AssetAdministrationShell
      val submodels = if (generateSubmodelIdentification) {
        var xmlReplaced = xml
        converter.parseSubmodels().forEach { submodel ->
          val regex = ">\\s*${escape(submodel.identification.id)}\\s*<".toRegex()
          val replacement = ">${aas!!.identification.id}-${submodel.idShort}<"
          xmlReplaced = xmlReplaced.replace(regex, replacement)
        }
        val converterReplaced = XMLToMetamodelConverter(xmlReplaced)
        aas = converterReplaced.parseAAS().first() as AssetAdministrationShell
        converterReplaced.parseSubmodels().filterIsInstance<Submodel>()
      } else {
        converter.parseSubmodels().filterIsInstance<Submodel>()
      }

      submodels.forEach { submodel ->
        basyx.addSubmodel(submodel)
      }
      basyx.addShell(aas)
      LOG.info("Created AAS with ID ${aas.identification.id}")
      ConverterResult(aas.identification.id, "Success", null, null)
    } catch (e: Exception) {
      LOG.warn("Failed to create AAS with identification ${aas?.identification?.id}")
      ConverterResult(aas?.identification?.id ?: "Unknown", e, debug)
    }
  }

  private fun readReplacementMap(input: InputStream): Map<String, List<XPathReplacement>> {
    val csvLines = input.readAllBytes().decodeToString().split(Regex("\r?\n")).toMutableList()
    val separator = when {
      csvLines.first().startsWith("sep=") -> csvLines.removeFirst().substring(4)
      csvLines.first().contains(';') -> ";"
      else -> ","
    }
    val cleanCsv = csvLines.filter { it.trim() != "" }.map {
      it.split(separator)
    }
    return cleanCsv.subList(2, cleanCsv.size).associate {
      it[1] to it.subList(1, it.size).mapIndexed { index, value ->
        XPathReplacement(cleanCsv[0][index + 1], cleanCsv[1][index + 1], value)
      }
    }
  }

  private fun replace(xmlDocument: Document, expression: String, replacement: String): Int {
    val nodeList =
      xPath.compile(expression.replace("/text()", "")).evaluate(xmlDocument, XPathConstants.NODESET) as NodeList
    if (nodeList.length == 0) {
      System.err.println("No matches found in Base AASX for XPath expression: $expression")
    }
    for (i in 0 until nodeList.length) {
      val parent = nodeList.item(i)
      if (parent.firstChild == null) {
        parent.appendChild(xmlDocument.createTextNode(replacement))
      } else {
        parent.firstChild.nodeValue = replacement
      }
    }
    return nodeList.length
  }
}