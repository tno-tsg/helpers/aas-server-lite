package nl.tsg.aas

import io.ktor.http.*
import io.ktor.serialization.gson.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.plugins.statuspages.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import nl.tsg.aas.basyx.FileSystemBasyx
import nl.tsg.aas.basyx.InMemoryBasyx
import nl.tsg.aas.basyx.MongoDBBasyx
import org.slf4j.LoggerFactory

class NotFoundException(message: String?): Exception(message)
class BadRequestException(message: String?): Exception(message)


data class ResultMessage(val success: Boolean, val isException: Boolean, val messages: List<Message>)

data class Message(val messageType: MessageType, val code: String, val text: String)

@Suppress("unused")
enum class MessageType(val printableName: String) {
  UNSPECIFIED("Unspecified"),
  DEBUG("Debug"),
  INFORMATION("Information"),
  WARNING("Warning"),
  ERROR("Error"),
  FATAL("Fatal"),
  EXCEPTION("Exception")
}

fun main() {
  val logger = LoggerFactory.getLogger("APIServer")

  val basyx = when (System.getenv("STORAGE_BACKEND")) {
    "MEMORY" -> {
      logger.warn("******************")
      logger.warn("")
      logger.warn("Starting with in-memory storage of shells, data is NOT persisted on restarts of the server")
      logger.warn("")
      logger.warn("******************")
      InMemoryBasyx()
    }
    "FILESYSTEM" -> {
      logger.warn("******************")
      logger.warn("")
      logger.warn("Starting with file-system storage of shells at ${System.getenv("BASYX_PATH") ?: "/resources"}, data is NOT persisted on restarts of the server in case the folder is not persisted outside the application.")
      logger.warn("")
      logger.warn("******************")
      FileSystemBasyx()
    }
    else -> {
      logger.info("Starting with MongoDB storage of shells")
      MongoDBBasyx(MongoDB())
    }
  }

  val converter = Converter(basyx)
  val port = System.getenv("BaSyxContext_contextPort")?.toInt() ?: 4001
  val contextPath = System.getenv("BaSyxContext_contextPath") ?: "/aasServer"
  logger.info("Setting up AAS Server on http://localhost:$port$contextPath")
  embeddedServer(Netty, port) {
    install(IgnoreTrailingSlash)
    install(ContentNegotiation) {
      gson {
        setPrettyPrinting()
      }
    }
    install(StatusPages) {
      exception<NotFoundException> { call, cause ->
        logger.warn("nl.tsg.aas.lite.NotFoundException: ${cause.message}")
        call.respond(HttpStatusCode.NotFound,
          ResultMessage(
            success = false,
            isException = false,
            messages = listOf(
              Message(
                MessageType.WARNING,
                "404",
                "Not Found: ${cause.message}"
              )
            )
          )
        )
      }
      exception<BadRequestException> { call, cause ->
        logger.warn("nl.tsg.aas.lite.BadRequestException: ${cause.message}")
        call.respond(HttpStatusCode.BadRequest,
          ResultMessage(
            success = false,
            isException = true,
            messages = listOf(
              Message(
                MessageType.ERROR,
                "400",
                "Bad Request: ${cause.message}"
              )
            )
          )
        )
      }
      exception<Exception> { call, cause ->
        logger.warn("Exception: ${cause.message}")
        call.respond(HttpStatusCode.InternalServerError,
          ResultMessage(
            success = false,
            isException = true,
            messages = listOf(
              Message(
                MessageType.EXCEPTION,
                "500",
                "Internal Server Error: ${cause.message}"
              )
            )
          )
        )
      }
    }
    install(Routing) {
      route(contextPath) {
        post("/upload") {
          call.respond(converter.handleRequest(call.receiveMultipart(), call.request.queryParameters["debug"] == "true"))
        }
        get("/shells") {
          call.respondTextWriter(ContentType.Application.Json) {
            basyx.listShells(call.request.queryParameters["identificationFilter"], call.request.queryParameters["compact"], this)
          }
        }
        get("/shells/{aasId}") {
          call.respondText(ContentType.Application.Json) {
            basyx.getShell(call.parameters["aasId"]!!)
          }
        }
        put("/shells/{aasId}") {
          call.respondText(ContentType.Application.Json, status = HttpStatusCode.Created) {
            basyx.addShell(call.parameters["aasId"]!!,call.receiveText())
          }
        }
        delete("/shells/{aasId}") {
          call.respondText(ContentType.Application.Json) {
            basyx.removeShell(call.parameters["aasId"]!!)
            ""
          }
        }
        get("/shells/{aasId}/aas") {
          call.respondText(ContentType.Application.Json) {
            basyx.getShell(call.parameters["aasId"]!!)
          }
        }
        get("/shells/{aasId}/aas/submodels") {
          call.respondText(ContentType.Application.Json) {
            basyx.getSubmodels(call.parameters["aasId"]!!)
          }
        }
        get("/shells/{aasId}/aas/submodels/{submodelIdShort}") {
          call.respondText(ContentType.Application.Json) {
            basyx.getSubmodel(call.parameters["aasId"]!!, call.parameters["submodelIdShort"]!!)
          }
        }
        put("/shells/{aasId}/aas/submodels/{submodelIdShort}") {
          call.respondText(ContentType.Application.Json, status = HttpStatusCode.Created) {
            basyx.addSubmodel(call.parameters["aasId"]!!, call.parameters["submodelIdShort"]!!, call.receiveText())
          }
        }
        delete("/shells/{aasId}/aas/submodels/{submodelIdShort}") {
          call.respondText(ContentType.Application.Json, status = HttpStatusCode.NoContent) {
            basyx.removeSubmodel(call.parameters["aasId"]!!, call.parameters["submodelIdShort"]!!)
            ""
          }
        }
        get("/shells/{aasId}/aas/submodels/{submodelIdShort}/submodel") {
          call.respondText(ContentType.Application.Json) {
            basyx.getSubmodel(call.parameters["aasId"]!!, call.parameters["submodelIdShort"]!!)
          }
        }
        get("/shells/{aasId}/aas/submodels/{submodelIdShort}/submodel/values") {
          call.respondText(status = HttpStatusCode.NotImplemented){""}
        }
        get("/shells/{aasId}/aas/submodels/{submodelIdShort}/submodel/submodelElements") {
          call.respondText(ContentType.Application.Json) {
            basyx.getSubmodelElements(call.parameters["aasId"]!!, call.parameters["submodelIdShort"]!!)
          }
        }
        post("/shells/{aasId}/aas/submodels/{submodelIdShort}/submodel/submodelElements/{idShortPathToOperation...}/invoke") {
          call.respondText(status = HttpStatusCode.NotImplemented){""}
        }
        get("/shells/{aasId}/aas/submodels/{submodelIdShort}/submodel/submodelElements/{seIdShortPath...}") {
          val aasId = call.parameters["aasId"]!!
          val submodelIdShort = call.parameters["submodelIdShort"]!!
          val seIdShortPath = call.parameters.getAll("seIdShortPath")!!
          when {
            call.parameters.getAll("seIdShortPath")?.last() == "value" ->
              call.respondText(ContentType.Application.Json) {
                basyx.getSubmodelElementValue(aasId, submodelIdShort, seIdShortPath.dropLast(1))
              }
            call.parameters.getAll("seIdShortPath")?.contains("invocationList") == true ->
              call.respondText(status = HttpStatusCode.NotImplemented){""}
            else ->
              call.respondText(ContentType.Application.Json) {
                basyx.getSubmodelElement(aasId, submodelIdShort, seIdShortPath)
              }
          }
        }
        put("/shells/{aasId}/aas/submodels/{submodelIdShort}/submodel/submodelElements/{seIdShortPath...}") {
          val aasId = call.parameters["aasId"]!!
          val submodelIdShort = call.parameters["submodelIdShort"]!!
          val seIdShortPath = call.parameters.getAll("seIdShortPath")!!
          val receiveText = call.receiveText()
          call.respondText(ContentType.Application.Json, status = HttpStatusCode.Created) {
            when {
              call.parameters.getAll("seIdShortPath")?.last() == "value" -> {
                basyx.addSubmodelElementValue(aasId, submodelIdShort, seIdShortPath.dropLast(1), receiveText)
                ""
              }
              else -> {
                basyx.addSubmodelElement(aasId, submodelIdShort, seIdShortPath, receiveText)
              }
            }
          }
        }
        delete("/shells/{aasId}/aas/submodels/{submodelIdShort}/submodel/submodelElements/{seIdShortPath...}") {
          val aasId = call.parameters["aasId"]!!
          val submodelIdShort = call.parameters["submodelIdShort"]!!
          val seIdShortPath = call.parameters.getAll("seIdShortPath")!!
          call.respondText(ContentType.Application.Json, status = HttpStatusCode.NoContent) {
            basyx.deleteSubmodelElement(aasId, submodelIdShort, seIdShortPath)
            ""
          }
        }
      }
    }
  }.start(wait = true)
}

