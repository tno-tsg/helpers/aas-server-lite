package nl.tsg.aas.basyx

import com.google.gson.GsonBuilder
import com.google.gson.stream.JsonWriter
import com.mongodb.client.model.Filters.*
import com.mongodb.client.model.Indexes
import com.mongodb.client.model.UpdateOptions
import kotlinx.coroutines.runBlocking
import nl.tsg.aas.*
import org.bson.Document
import org.bson.conversions.Bson
import org.eclipse.basyx.aas.metamodel.map.AssetAdministrationShell
import org.eclipse.basyx.submodel.metamodel.api.submodelelement.ISubmodelElement
import org.eclipse.basyx.submodel.metamodel.facade.SubmodelElementMapCollectionConverter
import org.eclipse.basyx.submodel.metamodel.facade.submodelelement.SubmodelElementFacadeFactory
import org.eclipse.basyx.submodel.metamodel.map.Submodel
import org.eclipse.basyx.submodel.metamodel.map.submodelelement.SubmodelElement
import org.eclipse.basyx.submodel.metamodel.map.submodelelement.SubmodelElementCollection
import org.litote.kmongo.EMPTY_BSON
import org.litote.kmongo.and
import org.litote.kmongo.excludeId
import org.litote.kmongo.fields
import org.slf4j.LoggerFactory
import java.io.Writer
import kotlin.text.Regex.Companion.escape

class MongoDBBasyx(private val mongoDB: MongoDB): Basyx() {
  companion object {
    private val LOG = LoggerFactory.getLogger(MongoDBBasyx::class.java)
  }

  init {
    runBlocking {
      mongoDB.aasCollection.createIndex(Indexes.ascending("identification.id"))
      mongoDB.submodelCollection.createIndex(Indexes.ascending("identification.id"))
    }
  }


  private fun idFilter(id: String): Bson {
    return eq("identification.id", id)
  }

  override suspend fun listShells(identificationFilter: String?, compactParam: String?, writer: Writer) {
    val compact = compactParam?.let { it.lowercase() != "false" } ?: false
    val filter = identificationFilter?.let {
      regex("identification.id", ".*${escape(identificationFilter)}.*")
    } ?: EMPTY_BSON
    val gson = GsonBuilder().setPrettyPrinting().create()
    val jsonWriter = JsonWriter(writer)
    jsonWriter.beginArray()
    mongoDB.aasCollection.find(filter).projection(fields(excludeId())).batchSize(1000).consumeEach {
      if (compact) {
        jsonWriter.value((it["identification"] as Document).getString("id"))
      } else {
        gson.toJson(it, Map::class.java, jsonWriter)
      }
    }
    jsonWriter.endArray()
    jsonWriter.close()
  }


  override suspend fun getShell(aasId: String): String {
    return mongoDB.aasCollection.find(idFilter(aasId)).projection(fields(excludeId())).limit(1).first()?.toGson()
      ?: throw NotFoundException("AAS with Identification $aasId not found")
  }

  override suspend fun addShell(aas: AssetAdministrationShell) {
    mongoDB.aasCollection.updateOne(
      eq("identification.id", aas.identification.id),
      aas,
      UpdateOptions().upsert(true)
    )
  }

  override suspend fun addShell(aasId: String, receiveText: String): String {
    try {
      val aas = gsonTools.deserialize(receiveText).toAas()
      mongoDB.aasCollection.updateOne(idFilter(aasId), aas, UpdateOptions().upsert(true))
      LOG.info("Added shell with identification ${aas.identification.id}")
      return aas.toGson()
    } catch (e: Exception) {
      LOG.warn("Error in adding AAS ${aasId}:  ${e.message}")
      throw BadRequestException(e.message)
    }
  }

  override suspend fun removeShell(aasId: String): ResultMessage {
    return if (mongoDB.aasCollection.deleteOne(idFilter(aasId)).deletedCount == 0L) {
      throw NotFoundException("AAS with Identification $aasId not found")
    } else {
      ResultMessage(
        success = true,
        isException = false,
        messages = listOf(Message(MessageType.INFORMATION, "200", "AAS with Identification $aasId not found removed "))
      )
    }
  }

  override suspend fun getSubmodels(aasId: String): String {
    val aas = retrieveAas(aasId)
    val idFilters = aas.submodelReferences.map { idFilter(it.keys.first().value) }
    val filter = or(idFilters)
    return mongoDB.submodelCollection.find(filter).projection(fields(excludeId())).limit(idFilters.size).batchSize(10).toList().map {
      SubmodelElementMapCollectionConverter.smToMap(it.toSubmodel())
    }.toGson()
  }

  override suspend fun getSubmodel(aasId: String, submodelIdShort: String): String {
    val submodel = retrieveSubmodel(aasId, submodelIdShort)
    return SubmodelElementMapCollectionConverter
      .smToMap(submodel)
      .toGson()
  }

  override suspend fun getSubmodelValues(aasId: String, submodelIdShort: String): String {
    return retrieveSubmodel(aasId, submodelIdShort).values.toGson()
  }

  override suspend fun getSubmodelElements(aasId: String, submodelIdShort: String): String {
    return retrieveSubmodel(aasId, submodelIdShort).submodelElements.values.toGson()
  }

  override suspend fun addSubmodel(submodel: Submodel) {
    mongoDB.submodelCollection.updateOne(
      eq("identification.id", submodel.identification.id),
      submodel,
      UpdateOptions().upsert(true)
    )
  }

  override suspend fun addSubmodel(aasId: String, submodelIdShort: String, receiveText: String): String {
    val aas = retrieveAas(aasId)
    try {
      val submodel = gsonTools.deserialize(receiveText).toSubmodel()
      submodel.idShort = submodelIdShort

      val idFilters = aas.submodelReferences.map {
        and(
          listOf(idFilter(it.keys.first().value), eq("idShort", submodelIdShort))
        )
      }
      if (idFilters.isEmpty()) {
        aas.submodelReferences = aas.submodelReferences + submodel.reference
      } else {
        val filter = or(idFilters)
        mongoDB.submodelCollection.find(filter).limit(1).first()?.toSubmodel()?.let { foundSubmodel ->
          if (foundSubmodel.identification.id != submodel.identification.id) {
            aas.submodelReferences = aas.submodelReferences.filter {
              it.keys.first().value != foundSubmodel.identification.id
            } + submodel.reference
          }
        } ?: run {
          aas.submodelReferences = aas.submodelReferences + submodel.reference
        }
      }
      mongoDB.aasCollection.updateOne(idFilter(aasId), aas)
      mongoDB.submodelCollection.updateOne(idFilter(submodel.identification.id), submodel, UpdateOptions().upsert(true))
      LOG.info("Added submodel $submodelIdShort in $aasId")
      return submodel.toGson()
    } catch (e: Exception) {
      LOG.warn("Error in adding submodel $submodelIdShort in $aasId: ${e.message}")
      throw BadRequestException(e.message)
    }
  }

  override suspend fun removeSubmodel(aasId: String, submodelIdShort: String) {
    val aas = retrieveAas(aasId)
    val idFilters = aas.submodelReferences.map {
      and(listOf(idFilter(it.keys.first().value), eq("idShort", submodelIdShort)))
    }
    val filter = or(idFilters)
    mongoDB.submodelCollection.findOneAndDelete(filter)?.toSubmodel()?.let { deletedSubmodel ->
      aas.submodelReferences = aas.submodelReferences.filter {
        it.keys.first().value == deletedSubmodel.identification.id
      }
      mongoDB.aasCollection.updateOne(idFilter(aasId), aas)
      LOG.info("Removed submodel $submodelIdShort in $aasId")
    } ?: run {
      LOG.warn("Error in removing submodel $submodelIdShort in $aasId: Not Found")
      throw NotFoundException("Submodel $submodelIdShort not found in AAS with Identification $aasId")
    }
  }

  override suspend fun getSubmodelElement(aasId: String, submodelIdShort: String, seIdShortPath: List<String>): String {
    val submodelElement = retrieveSubmodelElement(aasId, submodelIdShort, seIdShortPath)
    return if (submodelElement is SubmodelElementCollection) {
      SubmodelElementMapCollectionConverter
        .smElementToMap(submodelElement)
        .toGson()
    } else {
      submodelElement.toGson()
    }
  }

  override suspend fun getSubmodelElementValue(aasId: String, submodelIdShort: String, seIdShortPath: List<String>): String {
    val submodelElement = retrieveSubmodelElement(aasId, submodelIdShort, seIdShortPath)
    return submodelElement.value.toGson()
  }

  override suspend fun addSubmodelElement(aasId: String, submodelIdShort: String, seIdShortPath: List<String>, receiveText: String): String {
    val submodel = retrieveSubmodel(aasId, submodelIdShort)
    val element = try {
      gsonTools.deserialize(receiveText).toSubmodelElement()
    } catch (e: Exception) {
      LOG.warn("Error in adding submodelElement $seIdShortPath in submodel $submodelIdShort in $aasId: ${e.message}")
      throw BadRequestException(e.message)
    }
    when (seIdShortPath.size) {
      1 -> {
        submodel.addSubmodelElement(element)
        mongoDB.submodelCollection.updateOne(idFilter(submodel.identification.id), submodel)
      }
      else -> {
        var submodelElements = submodel.submodelElements[seIdShortPath.first()]
        for (elementIdShort in seIdShortPath.drop(1).dropLast(1)) {
          when (submodelElements) {
            is SubmodelElementCollection -> {
              submodelElements = submodelElements.submodelElements[elementIdShort]
            }
            else -> {
              LOG.warn("Error in adding submodelElement $seIdShortPath in submodel $submodelIdShort in $aasId: Submodel Element ID $elementIdShort not found")
              throw NotFoundException("$elementIdShort in the nested submodel element path could not be resolved")
            }
          }
        }
        when (submodelElements) {
          is SubmodelElementCollection -> {
            submodelElements.addSubmodelElement(element)
            mongoDB.submodelCollection.updateOne(idFilter(submodel.identification.id), submodel)
          }
          else -> {
            LOG.warn("Error in adding submodelElement ${seIdShortPath.dropLast(1).last()} in submodel $submodelIdShort in $aasId: Submodel Element is not an SubmodelElementCollection")
            throw NotFoundException("${seIdShortPath.dropLast(1).last()} in the nested submodel element path is not an Submodel Element Collection")
          }
        }
      }
    }
    LOG.info("Added submodelElement $seIdShortPath in submodel $submodelIdShort in $aasId")
    return element.toGson()
  }

  override suspend fun addSubmodelElementValue(aasId: String, submodelIdShort: String, seIdShortPath: List<String>, receiveText: String) {
    val elementJson = gsonTools.deserialize(receiveText)
    val submodel = retrieveSubmodel(aasId, submodelIdShort)
    when (seIdShortPath.size) {
      1 -> {
        try {
          submodel.submodelElements[seIdShortPath.first()]?.value = elementJson
        } catch (e: Exception) {
          LOG.warn("Error in adding submodelElement value in $seIdShortPath in submodel $submodelIdShort in $aasId: ${e.message}")
          throw BadRequestException(e.message)
        }
        mongoDB.submodelCollection.updateOne(idFilter(submodel.identification.id), submodel)
      }
      else -> {
        var submodelElements = submodel.submodelElements[seIdShortPath.first()]
        for (elementIdShort in seIdShortPath.drop(1)) {
          when (submodelElements) {
            is SubmodelElementCollection -> {
              submodelElements = submodelElements.submodelElements[elementIdShort]
            }
            else -> {
              LOG.warn("Error in adding submodelElement value in $seIdShortPath in submodel $submodelIdShort in $aasId: Submodel Element ID $elementIdShort not found")
              throw NotFoundException("$elementIdShort in the nested submodel element path could not be resolved")
            }
          }
        }
        try {
          submodelElements?.value = elementJson
        } catch (e: Exception) {
          LOG.warn("Error in adding submodelElement value in $seIdShortPath in submodel $submodelIdShort in $aasId: ${e.message}")
          throw BadRequestException(e.message)
        }
        mongoDB.submodelCollection.updateOne(idFilter(submodel.identification.id), submodel)
      }
    }
    LOG.info("Added submodelElementValue in $seIdShortPath in submodel $submodelIdShort in $aasId")
  }

  override suspend fun deleteSubmodelElement(aasId: String, submodelIdShort: String, seIdShortPath: List<String>) {
    val submodel = retrieveSubmodel(aasId, submodelIdShort)
    when (seIdShortPath.size) {
      1 -> {
        try {
          submodel.deleteSubmodelElement(seIdShortPath.first())
        } catch (e: Exception) {
          LOG.warn("Error in deleting submodelElement $seIdShortPath in submodel $submodelIdShort in $aasId: ${e.message}")
          throw NotFoundException(e.message)
        }
        mongoDB.submodelCollection.updateOne(idFilter(submodel.identification.id), submodel)
      }
      else -> {
        var submodelElements = submodel.submodelElements[seIdShortPath.first()]
        for (elementIdShort in seIdShortPath.drop(1).dropLast(1)) {
          when (submodelElements) {
            is SubmodelElementCollection -> {
              submodelElements = submodelElements.submodelElements[elementIdShort]
            }
            else -> {
              LOG.warn("Error in deleting submodelElement $seIdShortPath in submodel $submodelIdShort in $aasId: Submodel Element Collection with ID ${seIdShortPath.drop(1).dropLast(1)} not found")
              throw NotFoundException("$elementIdShort in the nested submodel element path could not be resolved")
            }
          }
        }
        when (submodelElements) {
          is SubmodelElementCollection -> {
            try {
              submodelElements.deleteSubmodelElement(seIdShortPath.last())
            } catch (e: Exception) {
              LOG.warn("Error in deleting submodelElement $seIdShortPath in submodel $submodelIdShort in $aasId: Submodel Element ID ${seIdShortPath.last()} not found in SubmodelElementCollection")
              throw NotFoundException(e.message)
            }
            mongoDB.submodelCollection.updateOne(idFilter(submodel.identification.id), submodel)
          }
          else -> {
            LOG.warn("Error in deleting submodelElement in ${seIdShortPath.dropLast(1).last()} in submodel $submodelIdShort in $aasId: Submodel Element ${seIdShortPath.dropLast(1).last()} is not an SubmodelElementCollection")
            throw NotFoundException("${seIdShortPath.dropLast(1).last()} in the nested submodel element path is not an Submodel Element Collection")
          }
        }
      }
    }
    LOG.info("Deleted submodelElement $seIdShortPath in submodel $submodelIdShort in $aasId")
  }

  private suspend fun retrieveAas(aasId: String): AssetAdministrationShell {
    val aasMap = mongoDB.aasCollection.find(idFilter(aasId)).limit(1).first()
    return aasMap?.let {
      AssetAdministrationShell.createAsFacade(it)
    } ?: throw NotFoundException("AAS with Identification $aasId not found")
  }

  private suspend fun retrieveSubmodel(aasId: String, submodelIdShort: String): Submodel {
    val aas = retrieveAas(aasId)
    val idFilters = aas.submodelReferences.map {
      and(listOf(idFilter(it.keys.first().value), eq("idShort", submodelIdShort)))
    }
    val filter = or(idFilters)
    return mongoDB.submodelCollection.find(filter).projection(fields(excludeId())).limit(1).first()?.toSubmodel()
      ?: throw NotFoundException("Submodel $submodelIdShort not found in AAS with Identification $aasId")
  }

  private suspend fun retrieveSubmodelElement(aasId: String, submodelIdShort: String, seIdShortPath: List<String>): ISubmodelElement {
    val submodel = retrieveSubmodel(aasId, submodelIdShort)
    var submodelElements = submodel.submodelElements[seIdShortPath.first()]
    for (elementIdShort in seIdShortPath.drop(1)) {
      when (submodelElements) {
        is SubmodelElementCollection -> {
          submodelElements = submodelElements.submodelElements[elementIdShort]
        }
        else -> throw NotFoundException("$elementIdShort in the nested submodel element path could not be resolved")
      }
    }
    return submodelElements ?: throw NotFoundException("SubmodelElement $seIdShortPath not found in submodel $submodelIdShort in AAS with Identification $aasId")
  }
}