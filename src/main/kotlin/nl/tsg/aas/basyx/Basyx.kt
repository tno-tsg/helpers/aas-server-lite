package nl.tsg.aas.basyx

import nl.tsg.aas.ResultMessage
import org.eclipse.basyx.aas.metamodel.map.AssetAdministrationShell
import org.eclipse.basyx.submodel.metamodel.facade.submodelelement.SubmodelElementFacadeFactory
import org.eclipse.basyx.submodel.metamodel.map.Submodel
import org.eclipse.basyx.submodel.metamodel.map.submodelelement.SubmodelElement
import org.eclipse.basyx.vab.coder.json.serialization.DefaultTypeFactory
import org.eclipse.basyx.vab.coder.json.serialization.GSONTools
import java.io.Writer

sealed class Basyx {
  protected val gsonTools = GSONTools(DefaultTypeFactory())

  protected fun Any.toGson(): String {
    return try {
      gsonTools.serialize(this)
    } catch (e: Exception) {
      gsonTools.serialize(this.toString())
    }
  }

  abstract suspend fun listShells(identificationFilter: String?, compactParam: String?, writer: Writer)
  abstract suspend fun getShell(aasId: String): String
  abstract suspend fun addShell(aas: AssetAdministrationShell)
  abstract suspend fun addShell(aasId: String, receiveText: String): String
  abstract suspend fun removeShell(aasId: String): ResultMessage
  abstract suspend fun getSubmodels(aasId: String): String
  abstract suspend fun getSubmodel(aasId: String, submodelIdShort: String): String
  abstract suspend fun getSubmodelValues(aasId: String, submodelIdShort: String): String
  abstract suspend fun getSubmodelElements(aasId: String, submodelIdShort: String): String
  abstract suspend fun addSubmodel(submodel: Submodel)
  abstract suspend fun addSubmodel(aasId: String, submodelIdShort: String, receiveText: String): String
  abstract suspend fun removeSubmodel(aasId: String, submodelIdShort: String)
  abstract suspend fun getSubmodelElement(aasId: String, submodelIdShort: String, seIdShortPath: List<String>): String
  abstract suspend fun getSubmodelElementValue(aasId: String, submodelIdShort: String, seIdShortPath: List<String>): String
  abstract suspend fun addSubmodelElement(aasId: String, submodelIdShort: String, seIdShortPath: List<String>, receiveText: String): String
  abstract suspend fun addSubmodelElementValue(aasId: String, submodelIdShort: String, seIdShortPath: List<String>, receiveText: String)
  abstract suspend fun deleteSubmodelElement(aasId: String, submodelIdShort: String, seIdShortPath: List<String>)
}

@Suppress("UNCHECKED_CAST")
fun Any.toAas(): AssetAdministrationShell {
  return AssetAdministrationShell.createAsFacade(this as MutableMap<String, Any>?)
}
@Suppress("UNCHECKED_CAST")
fun Any.toSubmodel(): Submodel {
  return Submodel.createAsFacade(this as MutableMap<String, Any>?)
}
@Suppress("UNCHECKED_CAST")
fun Any.toSubmodelElement(): SubmodelElement {
  return SubmodelElementFacadeFactory.createSubmodelElement(this as MutableMap<String, Any>?) as SubmodelElement
}