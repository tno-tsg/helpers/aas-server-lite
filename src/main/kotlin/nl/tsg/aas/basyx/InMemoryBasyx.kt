package nl.tsg.aas.basyx

import com.google.gson.GsonBuilder
import com.google.gson.stream.JsonWriter
import nl.tsg.aas.*
import org.eclipse.basyx.aas.metamodel.map.AssetAdministrationShell
import org.eclipse.basyx.submodel.metamodel.api.submodelelement.ISubmodelElement
import org.eclipse.basyx.submodel.metamodel.facade.SubmodelElementMapCollectionConverter
import org.eclipse.basyx.submodel.metamodel.map.Submodel
import org.eclipse.basyx.submodel.metamodel.map.submodelelement.SubmodelElementCollection
import org.slf4j.LoggerFactory
import java.io.Writer
import java.util.concurrent.ConcurrentHashMap

class InMemoryBasyx: Basyx() {
  companion object {
    private val LOG = LoggerFactory.getLogger(InMemoryBasyx::class.java)
  }
  val aasMap: MutableMap<String, AssetAdministrationShell> = ConcurrentHashMap()
  val submodelMap: MutableMap<String, Submodel> = ConcurrentHashMap()

  override suspend fun listShells(identificationFilter: String?, compactParam: String?, writer: Writer) {
    val compact = compactParam?.let { it.lowercase() != "false" } ?: false

    val shells = if (identificationFilter != null) {
      aasMap.filterKeys { it.contains(identificationFilter) }.values
    } else {
      aasMap.values
    }

    val gson = GsonBuilder().setPrettyPrinting().create()
    val jsonWriter = JsonWriter(writer)
    jsonWriter.beginArray()
    shells.forEach {
      if (compact) {
        jsonWriter.value(it.identification.id)
      } else {
        gson.toJson(it, AssetAdministrationShell::class.java, jsonWriter)
      }
    }

    jsonWriter.endArray()
    jsonWriter.close()
  }

  override suspend fun getShell(aasId: String): String {
    return aasMap[aasId]?.toGson()
      ?: throw NotFoundException("AAS with Identification $aasId not found")
  }

  override suspend fun addShell(aas: AssetAdministrationShell) {
    aasMap[aas.identification.id] = aas
  }

  override suspend fun addShell(aasId: String, receiveText: String): String {
    try {
      val aas = gsonTools.deserialize(receiveText).toAas()
      aasMap[aasId] = aas
      return aas.toGson()
    } catch (e: Exception) {
      LOG.warn("Error in adding AAS ${aasId}:  ${e.message}")
      throw BadRequestException(e.message)
    }
  }

  override suspend fun removeShell(aasId: String): ResultMessage {
    return aasMap.remove(aasId)?.let {
      ResultMessage(
        success = true,
        isException = false,
        messages = listOf(Message(MessageType.INFORMATION, "200", "AAS with Identification $aasId not found removed "))
      )
    } ?: throw NotFoundException("AAS with Identification $aasId not found")
  }

  override suspend fun getSubmodels(aasId: String): String {
    return aasMap[aasId]?.submodelReferences?.map {
      val submodel = submodelMap[it.keys.first().value]
      SubmodelElementMapCollectionConverter.smToMap(submodel)
    }?.toGson() ?: throw NotFoundException("AAS with Identification $aasId not found")
  }

  override suspend fun getSubmodel(aasId: String, submodelIdShort: String): String {
    return retrieveSubmodel(aasId, submodelIdShort).toGson()
  }

  override suspend fun getSubmodelValues(aasId: String, submodelIdShort: String): String {
    return retrieveSubmodel(aasId, submodelIdShort).values.toGson()
  }

  override suspend fun getSubmodelElements(aasId: String, submodelIdShort: String): String {
    return retrieveSubmodel(aasId, submodelIdShort).submodelElements.values.toGson()
  }

  override suspend fun addSubmodel(submodel: Submodel) {
    submodelMap[submodel.identification.id] = submodel
  }

  override suspend fun addSubmodel(aasId: String, submodelIdShort: String, receiveText: String): String {
    val aas = aasMap[aasId] ?: throw NotFoundException("AAS with Identification $aasId not found")
    try {
      val submodel = gsonTools.deserialize(receiveText).toSubmodel()
      submodel.idShort = submodelIdShort

      val foundSubmodel = aas.submodelReferences
        .mapNotNull { submodelMap[it.keys.first().value] }
        .firstOrNull { it.idShort == submodelIdShort }

      if (foundSubmodel == null) {
        aas.submodelReferences = aas.submodelReferences + submodel.reference
      } else {
        if (foundSubmodel.identification.id != submodel.identification.id) {
          aas.submodelReferences = aas.submodelReferences.filter { it.keys.first().value != foundSubmodel.identification.id } + submodel.reference
        } else {
          aas.submodelReferences = aas.submodelReferences + submodel.reference
        }
      }

      submodelMap[submodel.identification.id] = submodel
      LOG.info("Added submodel $submodelIdShort in $aasId")

      return submodel.toGson()
    } catch (e: Exception) {
      LOG.warn("Error in adding submodel $submodelIdShort in $aasId: ${e.message}")
      throw BadRequestException(e.message)
    }
  }

  override suspend fun removeSubmodel(aasId: String, submodelIdShort: String) {
    retrieveSubmodel(aasId, submodelIdShort).identification.id.let  { submodelIdentification ->
      aasMap[aasId]?.let { aas ->
        aas.submodelReferences = aas.submodelReferences.filter { it.keys.first().value != submodelIdentification }
      }
      submodelMap.remove(submodelIdentification)
    }
  }

  override suspend fun getSubmodelElement(aasId: String, submodelIdShort: String, seIdShortPath: List<String>): String {
    val submodelElement = retrieveSubmodelElement(aasId, submodelIdShort, seIdShortPath)
    return if (submodelElement is SubmodelElementCollection) {
      SubmodelElementMapCollectionConverter
        .smElementToMap(submodelElement)
        .toGson()
    } else {
      submodelElement.toGson()
    }
  }

  override suspend fun getSubmodelElementValue(
    aasId: String,
    submodelIdShort: String,
    seIdShortPath: List<String>
  ): String {
    val submodelElement = retrieveSubmodelElement(aasId, submodelIdShort, seIdShortPath)
    return submodelElement.value.toGson()
  }

  override suspend fun addSubmodelElement(
    aasId: String,
    submodelIdShort: String,
    seIdShortPath: List<String>,
    receiveText: String
  ): String {
    val submodel = retrieveSubmodel(aasId, submodelIdShort)
    val element = try {
      gsonTools.deserialize(receiveText).toSubmodelElement()
    } catch (e: Exception) {
      LOG.warn("Error in adding submodelElement $seIdShortPath in submodel $submodelIdShort in $aasId: ${e.message}")
      throw BadRequestException(e.message)
    }
    when (seIdShortPath.size) {
      1 -> {
        submodel.addSubmodelElement(element)
      }
      else -> {
        var submodelElements = submodel.submodelElements[seIdShortPath.first()]
        for (elementIdShort in seIdShortPath.drop(1).dropLast(1)) {
          when (submodelElements) {
            is SubmodelElementCollection -> {
              submodelElements = submodelElements.submodelElements[elementIdShort]
            }
            else -> {
              LOG.warn("Error in adding submodelElement $seIdShortPath in submodel $submodelIdShort in $aasId: Submodel Element ID $elementIdShort not found")
              throw NotFoundException("$elementIdShort in the nested submodel element path could not be resolved")
            }
          }
        }
        when (submodelElements) {
          is SubmodelElementCollection -> {
            submodelElements.addSubmodelElement(element)
          }
          else -> {
            LOG.warn("Error in adding submodelElement ${seIdShortPath.dropLast(1).last()} in submodel $submodelIdShort in $aasId: Submodel Element is not an SubmodelElementCollection")
            throw NotFoundException("${seIdShortPath.dropLast(1).last()} in the nested submodel element path is not an Submodel Element Collection")
          }
        }
      }
    }
    LOG.info("Added submodelElement $seIdShortPath in submodel $submodelIdShort in $aasId")
    return element.toGson()
  }

  override suspend fun addSubmodelElementValue(
    aasId: String,
    submodelIdShort: String,
    seIdShortPath: List<String>,
    receiveText: String
  ) {
    val elementJson = gsonTools.deserialize(receiveText)
    val submodel = retrieveSubmodel(aasId, submodelIdShort)
    when (seIdShortPath.size) {
      1 -> {
        try {
          submodel.submodelElements[seIdShortPath.first()]?.value = elementJson
        } catch (e: Exception) {
          LOG.warn("Error in adding submodelElement value in $seIdShortPath in submodel $submodelIdShort in $aasId: ${e.message}")
          throw BadRequestException(e.message)
        }
      }
      else -> {
        var submodelElements = submodel.submodelElements[seIdShortPath.first()]
        for (elementIdShort in seIdShortPath.drop(1)) {
          when (submodelElements) {
            is SubmodelElementCollection -> {
              submodelElements = submodelElements.submodelElements[elementIdShort]
            }
            else -> {
              LOG.warn("Error in adding submodelElement value in $seIdShortPath in submodel $submodelIdShort in $aasId: Submodel Element ID $elementIdShort not found")
              throw NotFoundException("$elementIdShort in the nested submodel element path could not be resolved")
            }
          }
        }
        try {
          submodelElements?.value = elementJson
        } catch (e: Exception) {
          LOG.warn("Error in adding submodelElement value in $seIdShortPath in submodel $submodelIdShort in $aasId: ${e.message}")
          throw BadRequestException(e.message)
        }
      }
    }
    LOG.info("Added submodelElementValue in $seIdShortPath in submodel $submodelIdShort in $aasId")
  }

  override suspend fun deleteSubmodelElement(aasId: String, submodelIdShort: String, seIdShortPath: List<String>) {
    val submodel = retrieveSubmodel(aasId, submodelIdShort)
    when (seIdShortPath.size) {
      1 -> {
        try {
          submodel.deleteSubmodelElement(seIdShortPath.first())
        } catch (e: Exception) {
          LOG.warn("Error in deleting submodelElement $seIdShortPath in submodel $submodelIdShort in $aasId: ${e.message}")
          throw NotFoundException(e.message)
        }
      }
      else -> {
        var submodelElements = submodel.submodelElements[seIdShortPath.first()]
        for (elementIdShort in seIdShortPath.drop(1).dropLast(1)) {
          when (submodelElements) {
            is SubmodelElementCollection -> {
              submodelElements = submodelElements.submodelElements[elementIdShort]
            }
            else -> {
              LOG.warn("Error in deleting submodelElement $seIdShortPath in submodel $submodelIdShort in $aasId: Submodel Element Collection with ID ${seIdShortPath.drop(1).dropLast(1)} not found")
              throw NotFoundException("$elementIdShort in the nested submodel element path could not be resolved")
            }
          }
        }
        when (submodelElements) {
          is SubmodelElementCollection -> {
            try {
              submodelElements.deleteSubmodelElement(seIdShortPath.last())
            } catch (e: Exception) {
              LOG.warn("Error in deleting submodelElement $seIdShortPath in submodel $submodelIdShort in $aasId: Submodel Element ID ${seIdShortPath.last()} not found in SubmodelElementCollection")
              throw NotFoundException(e.message)
            }
          }
          else -> {
            LOG.warn("Error in deleting submodelElement in ${seIdShortPath.dropLast(1).last()} in submodel $submodelIdShort in $aasId: Submodel Element ${seIdShortPath.dropLast(1).last()} is not an SubmodelElementCollection")
            throw NotFoundException("${seIdShortPath.dropLast(1).last()} in the nested submodel element path is not an Submodel Element Collection")
          }
        }
      }
    }
    LOG.info("Deleted submodelElement $seIdShortPath in submodel $submodelIdShort in $aasId")
  }

  private fun retrieveSubmodel(aasId: String, submodelIdShort: String): Submodel {
    return aasMap[aasId]?.submodelReferences
      ?.map { submodelMap[it.keys.first().value] }
      ?.firstOrNull { it?.idShort == submodelIdShort }
      ?: throw NotFoundException("Submodel $submodelIdShort not found in AAS with Identification $aasId")
  }

  private fun retrieveSubmodelElement(aasId: String, submodelIdShort: String, seIdShortPath: List<String>): ISubmodelElement {
    val submodel = retrieveSubmodel(aasId, submodelIdShort)
    var submodelElements = submodel.submodelElements[seIdShortPath.first()]
    for (elementIdShort in seIdShortPath.drop(1)) {
      when (submodelElements) {
        is SubmodelElementCollection -> {
          submodelElements = submodelElements.submodelElements[elementIdShort]
        }
        else -> throw NotFoundException("$elementIdShort in the nested submodel element path could not be resolved")
      }
    }
    return submodelElements ?: throw NotFoundException("SubmodelElement $seIdShortPath not found in submodel $submodelIdShort in AAS with Identification $aasId")
  }
}