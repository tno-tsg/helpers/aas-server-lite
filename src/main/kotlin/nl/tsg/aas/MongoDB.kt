package nl.tsg.aas

import com.mongodb.ConnectionString
import com.mongodb.MongoClientSettings
import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.reactivestreams.KMongo

class MongoDB {
  private val mongoConnectionString = System.getenv("BaSyxMongoDB_dbconnectionstring") ?: System.getenv("MONGODB_CONNECTIONSTRING")
  private val mongoDatabase = System.getenv("BaSyxMongoDB_dbname") ?: System.getenv("MONGODB_DATABASE")
  private val client = KMongo.createClient(
    MongoClientSettings
      .builder()
      .applyConnectionString(ConnectionString(mongoConnectionString))
      .applyToConnectionPoolSettings {
        it.maxSize(10)
      }
      .build()
  ).coroutine
  private val database = client.getDatabase(mongoDatabase)
  val aasCollection = database.getCollection<Map<String, Any>>("assetadministrationshells")
  val submodelCollection = database.getCollection<Map<String, Any>>("submodels")
}